<?php
class SRContextStack extends SRArrayObject implements SRContextStackInterface {
  public function sanitize($string) {
    if(!$this->count()) {
      return $string;
    }
    $new_contextstack = clone $this;
    $context = $new_contextstack->array_shift();
    $decoded = $context->decode($string);
    $subsanitized = $new_contextstack->sanitize($decoded);
    $reencoded = $context->encode($subsanitized);
    $sanitized = $context->sanitize($reencoded);
    return $sanitized;
  }

}