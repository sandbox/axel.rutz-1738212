<?php
class SRTemplate implements SRTemplateInterface {
  protected $template;
  public function __construct($template) {
    $this->template = $template;
  }
  public function set_token_mappings(array $token_replacements) {
    return new SRTemplatePlugged($this->template, $token_replacements);
  }
}

class SRTemplatePlugged implements SRTemplatePluggedInterface {
  protected $template;
  protected $token_replacements;
  public function __construct($template, array $token_replacements) {
    $this->template = $template;
    $this->token_replacements = $token_replacements;
  }
  public function sanitizeContextually(SRContextStackInterface $contextstack_above) {
    // preprocess template, prepare $token_find_strings, $token_contextstacks
    // TODO: we may cache this.
    $token_find_strings = $token_contextstacks = array();
    // match all token occurences
    preg_match_all('#({.*?})#', $this->template, $matches);
    foreach($matches[1] as $token_find_string) {
      preg_match('#^{(.*?):(.*)}$#D', $token_find_string, $matches);
      // TODO: freak out if syntax error!
      $token_name = $matches[1];
      // TODO: freak out if variable not set or wrong type!
      $token_replacement = $this->token_replacements[$token_name];
      $token_contextstack = explode('+', $matches[2]);
      // look up context classes
      $token_contextstack = new SRContextStack(array_map('sr_context', $token_contextstack));
      $re_var = '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
      // TODO: freak out if syntax error!
      $token_find_strings[$token_find_string] = $token_replacement;
      $token_contextstacks[$token_find_string] = $token_contextstack;
    }
    // prepare $slot_replacements.
    $slot_replacements = array();
    foreach($token_find_strings as $token_find_string => $token_replacement) {
      // wrap $token_replacement in all contexts.
      $contextstack_below = $token_contextstacks[$token_find_string];

      // we have to wrap bottom-up, so reverse.
      foreach($contextstack_below as $context) {
        $token_replacement = new SRContextWrapper($context, $token_replacement);
      }
      // collect rendered replacements.
      $slot_replacements[$token_find_string] = $token_replacement->sanitizeContextually($contextstack_above);
    }
    // do the replace and return the result.
    return str_replace(array_keys($slot_replacements), $slot_replacements, $this->template);
  }
}
