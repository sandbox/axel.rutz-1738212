<?php
class SRArrayObject extends ArrayObject {
public function array_merge($array) {
    $result = array_merge((array)$this, (array)$array);
    $classname = get_class($this);
    $result = new $classname($result);
    return $result;
  }
  public function array_reverse() {
    $result = array_reverse((array)$this);
    $classname = get_class($this);
    $result = new $classname($result);
    return $result;
  }
  public function array_shift() {
    $array = $this->getArrayCopy();
    $result = array_shift($array);
    $this->exchangeArray($array);
    return $result;
  }
  public function array_map($callback) {
    $result = array_map($callback, $this);
    $classname = get_class($this);
    $result = new $classname($result);
    return $result;
  }
}
