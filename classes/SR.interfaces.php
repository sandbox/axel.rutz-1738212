<?php
/**
 * SR.interfaces.inc
 *
 * Interfaces for safe rendering.
 */

interface SRContextInterface {
  public function sanitize($string);
  public function decode($string);
  public function encode($string);
}


interface SRPlugInterface {
  // $contextstack is ordered top-down
  public function sanitizeContextually(SRContextStackInterface $contextstack);
}

interface SRRenderableInterface extends SRPlugInterface {
  public function render();
}


interface SRItemInterface extends SRPlugInterface {
  public function __construct($string);
}


interface SRTemplateInterface {
  public function __construct($template);
  public function set_token_mappings(array $variables);
}

interface SRTemplatePluggedInterface extends SRPlugInterface {
  public function __construct($template, array $variables);
}


interface SRContextWrapperInterface extends SRRenderableInterface {
  public function __construct(SRContextInterface $context, SRPlugInterface $plug);
}

interface SRContextStackInterface extends ArrayAccess, Countable, IteratorAggregate {
  public function __construct($array);
  public function sanitize($string);
}