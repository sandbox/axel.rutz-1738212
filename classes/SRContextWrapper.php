<?php
class SRContextWrapper implements SRContextWrapperInterface {
  private $context;
  private $plug;
  public function __construct(SRContextInterface $context, SRPlugInterface $plug) {
    $this->context = $context;
    $this->plug = $plug;
  }
  public function sanitizeContextually(SRContextStackInterface $contextstack) {
    $new_contextstack = clone $contextstack;
    $new_contextstack[] = $this->context;
    return $this->plug->sanitizeContextually($new_contextstack);
  }
  public function render() {
    return $this->sanitizeContextually(new SRContextStack());
  }
}
