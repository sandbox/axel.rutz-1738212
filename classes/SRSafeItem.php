<?php
class SRSafeItem implements SRItemInterface {
  private $string;
  public function __construct($string) {
    $this->string = $string;
  }
  public function sanitizeContextually(SRContextStackInterface $contextstack) {
    return $this->string;
  }
}
