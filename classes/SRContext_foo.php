<?php

/**
 * All the context classes
 */

/**
 * Debug helper
 */
function sr_debug($class, $method, $in, $out) {
  $msg = '%class::%method: %in => %out';
  $var = array(
    '%class' => $class,
    '%method' => $method,
    '%in' => $in,
    '%out' => $out,
  );
  drupal_set_message(t($msg, $var));
}

abstract class SRContext implements SRContextInterface {
  public function sanitize($string) {
    $return = $string;
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
  public function decode($string) {
    $return = $string;
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
  public function encode($string) {
    $return = $string;
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
}
class SRContext_html_fragment extends SRContext {
  public function sanitize($string) {
    $return = filter_xss($string);
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
  public function decode($string) {
    $return = html_entity_decode($string);
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
  public function encode($string) {
    $return = htmlspecialchars($string, ENT_NOQUOTES);
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
}

class SRContext_doublequoted_js extends SRContext {
  public function sanitize($string) {
    $return = str_replace('"', '\"', $string);
    // TODO: whatever else is to do here
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
}

class SRContext_singlequoted_js extends SRContext {
  public function sanitize($string) {
    $return = str_replace("'", "\'", $string);
    // TODO: whatever else is to do here
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
}

class SRContext_html_attribute_href extends SRContext {
  public function sanitize($string) {
    $return = str_replace('javascript:', '(sanitized)', $string);
    sr_debug(get_class($this), __FUNCTION__, $string, $return);
    return $return;
  }
}
